### CONTAINER is a running enviroment for IMAGE
<br>

### check existing images

<br>

```
docker image ls
```
<br>

```
docker run 'image name'
```

### Get Container ID

<br>

```
docker run -d 'image name'
```


### Stop Container

<br>

```
docker stop 'container id'
```

### show running Containers

<br>

```
docker ps
```

### Show all Containers

<br>

```
docker ps -a
```

<br>


# Docker Compose

<br>

### Remove images

<br>

```
docker image rm -f $(docker image ls -q)
```

### Get images ids

<br>

```
docker image ls -q
```

### Remove Containers

<br>

```
docker container rm -f $(docker conatiner ls -aq)
```

--------------------------------------

<br>


## Creating Docker Compose file


<br>

```
default name: docker-compose.yml

version: "3.8" // the latest version

services:
    frontend:
        build: ./frontend  ( route to Docker file )
        ports: ( -: list)
            -3000:3000  ( host port:container port )

    backend:
        build: ./backend
        ports:
            - 3001:3001
        environment:
            DB_URL: mongodb://host name   (where our DB is)
            
    database:
        ( for the database we need to pull an images from docker hub )

        image: mongo:4.0-xenial (the image name)
        ports:
            - 27017:27017
        volumes:
            - volumeName:/data/db ( map the volume to directory in side the container )

Define volume in compose file
volumes:
    volumeName:

```

### show all docker-cpmpose commande

<br>

```
docker-compose
```

### Build images

<br>

```
docker-compose build
```

### Starting and Stopping the application

<br>

```
docker-compose up -d ( start container in the background )

docker-compose down ( stop and remove containers )
```