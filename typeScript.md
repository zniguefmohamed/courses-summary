# typeScript

- ## type

<br>

```
type MyObject = {
  first: number;
  second: number;
}


export const addTwoNumbers = (params: MyObject) => {
return params.first + params.second;
};

```

<br>

- ## interface

<br>

```
interface MyName {
  firstName: string;
  lastName: string;
}

export const fullName = (myName: MyName ) => {
return `${myName.firstName } ${myName.lastName}`;
};

```

<br>

- ## optional-properties ?

<br>

```
type getUserName = {
  first: string;
  last?: string
}

export const getName = (userParams: getUserName ) => {
if (userParams.last) {
return `${userParams.first} ${userParams.last}`;
}
return userParams.first;
};
```

<br>

- ## assigning-types-to-variables

<br>

```
type User = {
  id: number;
  firstName: string;
  lastName: string;
  isAdmin: boolean;
}

const adminUser: User = {
  id: 1,
  firstName: "me",
  lastName: "me_me",
  isAdmine: true,
}

const getAdminId = (user: User) => {
return user.id;
}
```

- ## unions

<br>

```
interface User {
  id: number;
  firstName: string;
  lastName: string;
  role: "admin" | "user" | "super_admin";
}
```

- ## Array

<br>

```
interface User {
  id: number;
  firstName: string;
  lastName: string;
  role: "admin" | "user" | "super-admin";
  posts: Post[];

  posts: Array<Post>; // You can also use it like this

}

interface Post {
  id: number;
  title: string;
}

export const defaultUser: User = {
  id: 1,
  firstName: "Matt",
  lastName: "Pocock",
  role: "admin",
  posts: [
    {
      id: 1,
      title: "How I eat so much cheese",
    },
    {
      id: 2,
      title: "Why I don't eat more vegetables",
    },
  ],
};
```

<br>

- ## promises

<br>

```
interface Person {
  name: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
}

export const fetchLukeSkywalker = async (): Promise<Person> => {
  const data = await fetch("https://website/api/people/1").then((res) => {
  return res.json();
});

 return data;
};
```
