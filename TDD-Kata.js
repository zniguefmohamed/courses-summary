// 1
const stringCalculator = (function () {
  function add(numberString) {
    if (numberString === "") {
      return 0;
    }

    const delimiter = getDelimiter(numberString);
    const formattedInput = formatInput(numberString);
    return calculateSum(getNumbers(formattedInput, delimiter));
  }

  function formatInput(input) {
    const delimiterRegExp = /^(\/\/.*\n)/;
    const matches = delimiterRegExp.exec(input);
    if (matches && matches.length > 0) {
      return input.replace(delimiterRegExp, "");
    }
    return input;
  }

  function getDelimiter(input) {
    const delimiters = [];
    const multipleDelimiterRegexp = /(?:^\/\/)?\[([^\[\]]+)\]\n?/g;
    let matches = multipleDelimiterRegexp.exec(input);
    while (matches !== null) {
      delimiters.push(matches[1]);
      matches = multipleDelimiterRegexp.exec(input);
    }
    if (delimiters.length > 0) {
      return new RegExp("[" + delimiters.join("") + "]");
    }
    matches = /^\/\/(.*)\n/.exec(input);
    if (matches && matches[1]) {
      return matches[1];
    }
    return /[\n,]/;
  }

  function getNumbers(string, delimiter) {
    return string
      .toString()
      .split(delimiter)
      .filter((n) => n !== "")
      .map((n) => parseInt(n));
  }

  function calculateSum(numbers) {
    const negatives = [];
    const finalSum = numbers.reduce((sum, n) => {
      if (n > 1000) {
        return 0;
      }
      if (n < 0) {
        negatives.push(n);
        return ("Negatives not allowed");
      }
      return sum + n;
    }, 0);
    return finalSum;
  }
  

  return { add };
})();

console.log(stringCalculator.add("")); // 0
console.log(stringCalculator.add("1,2,3")); // 6
console.log(stringCalculator.add('1\n2,3')); // 6
console.log(stringCalculator.add('-1,-2,-3')); // Negatives not allowed
console.log(stringCalculator.add('//;\n1;2')) // 3
console.log(stringCalculator.add('2 + 1001')) // 2

module.exports = stringCalculator;
