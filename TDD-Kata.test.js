const stringCalculator = require("./TDD-Kata");


describe('stringCalculator', () => {

    // Test1  for empty string it will return 0
    it('it should return 0', () => {
        expect(stringCalculator.add("")).toBe(0);
    })

    // Test2  take numbers and  return their sum
    it('it should return sum', () => {
        expect(stringCalculator.add("1,2,3")).toBe(6);
    })

    // Test3 Return Negatives not allowed
    it('it should return negatives not allowed', () => {
        expect(stringCalculator.add("-1, -5, -5")).toBe("Negatives not allowed")
    })

    // Test4 take numbers caracter and symbols and return the numbers sum
    it('it should return 3', () => {
        expect(stringCalculator.add('//;\n1;2')).toBe(3)
    })

    // Test5 Numbers bigger than 1000 should be ignored
    it('should return 2', () => {
        expect(stringCalculator.add('2 + 1001')).toBe(2)
    })

})