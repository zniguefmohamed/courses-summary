# React Summary

***All React apps require three things:***

>ReactDOM.render() : used to render (show) our app by mounting it onto an HTML element

<br>

>A JSX element: called a **root node**, because it is the root of
our application. Meaning, rendering it will render all
children within it

<br>

>An HTML **DOM** element: Where the app is inserted within an HTML page. The element is usually a div with an id of
**root**, located in an index.html file

<br>

```
import React from "react";

import ReactDOM from "react-dom";

const App = <h1>Hello React!</h1>;

// ReactDOM.render(root node, HTML element)
ReactDOM.render(App, document.getElementById("root"));
```

<br>


> **JSX** is the most common way to structure React applications, but JSX is not required for React

<br>

```JSX
JSX is a simpler way to use the function React.createElement()
In other words.

the following two lines in React are the same:

<div>Hello React!</div> // JSX syntax

React.createElement(
    'div', 
    null, 
    'Hello React!'
); // createElement syntax
```

<br>

>  Some JSX attributes are named differently than HTML attributes. Why? <br>
> Because some attribute words are reserved words in JavaScript
Also, because JSX is JavaScript, attributes that consist of multiple words are written in camelcase

<br>

```
<div id="header">
<h1 className="title">Hello React!</h1>
</div>
```

<br>

> Inline styles can be added to JSX elements using the **style** attribute <br>
> style property names must be also written in camelcase

<br>


```
<h1 style={{ color: "blue", fontSize: 22, padding: "0.5em 1em" }}>
    Hello React!
</h1>;

```

<br>

> Expressions can also be used for element attributes

<br>

```
const className = "title";

const title = <h1 className={className}>My title</h1>;
```

<br>

## Components and Props

<br>

* JSX can be grouped together within individual functions called components

* There are two types of components in React: **function components** and **class components**
  
* Component names, for function or class components, are capitalized to distinguish them from plain JavaScript
functions that do not return JSX

<br>

## Function component

<br>

```
function Header() {
return <h1>Hello React</h1>;
}

const Header = () => <h1>Hello React</h1>;

```

## Class component

<br>

```
class Header extends React.Component {

    render() {
        return <h1>Hello React</h1>;
    }
}
```

<br>

* Data passed to components in JavaScript are called props

* Props look identical to attributes on plain JSX/HTML elements, but you can access their values within the component
itself

* Props are available in parameters of the component to which they are passed. Props are always included as properties
of an object

<br>

* To use conditions within a component's returned JSX, you can use the ternary operator or short-circuiting ( && and || operators )

<br>


```
function Header() {

const isAuthenticated = checkAuth();
    return (
        <nav>
            { if isAuth is true, show nothing. If false, show Logo }

            {isAuthenticated || <Logo />}

            { if isAuth is true, show AuthenticatedApp. If false, show Login }

            {isAuthenticated ? <AuthenticatedApp /> : <LoginScreen />}

            { if isAuth is true, show Footer. If false, show nothing }

            {isAuthenticated && <Footer />}
        </nav>
    );
}
```

<br>

* Fragments are special components for displaying multiple components without adding an extra element to the DOM

```
We can render both components with a fragment.
Fragments are very concise: <> </>

<>
    <AuthenticatedApp />
    <Footer />
</>

```

<br>

## Lists and Keys

<br>

* Use the **.map()** function to convert lists of data (arrays) into lists of elements

<br>

```
const people = ["John", "Bob", "Fred"];
const peopleList = people.map((person) => <p>{person}</p>);
```

* React uses keys to performantly update individual elements when their data changes (instead of re-rendering the
entire list)

* Keys need to have unique values to be able to identify each of them according to their key value

<br>

## Event Listeners and Handling Events

<br>

**The most essential React events to know are onClick , onChange , and onSubmit** 

* onClick handles click events on JSX elements (namely on buttons)

* onChange handles keyboard events (namely a user typing into an input or textarea)

* onSubmit handles form submissions from the user

<br>

## Essential React Hooks

<br>

**State and useState**

* useState give us state in a function component

* State allows us to access and update certain values in our components over time

<br>

```
const [language, setLanguage] = React.useState("javascript");
```

<br>

**Side effects and useEffect**

<br>

* useEffect lets us perform side effects in function components. What are side effects?

* Side effects are where we need to reach into the outside world. For example, fetching data from an API or working
with the DOM


