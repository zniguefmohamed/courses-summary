# Git summary

- ## Initialize local repository

<br>

```
git init
```

- ## Add all html files to the staging area

<br>

```
git add *.html
```

- ## Add file(s) to staging area

<br>

```
git add (.)
```

- ## Commit changes from staging area to the local repo

<br>

```
git commit -m "your message"
```

- ## Remove file from staging area

<br>

```
 git rm --cached "file name"
```

- ## Push to remote repository

<br>

```
git push
```

- ## Pull latest from remote to local repo

<br>

```
git pull
```

- ## Copy remote repo to your folder

<br>

```
git clone
```

- ## Create a new branch and move to it

<br>

```
git checkout -b 'branch name'
```

- ## Create new branch in current branch

<br>

```
git branch 'branch name'
```

- ## List all branch

<br>

```
git branch | git branch --list
```

- ## Switch from branch to other branch

<br>

```
git checkout 'branch name'
```

- ## Rename the current branch

<br>

```
git branch -m 'name'
```

- ## Delete local branch

<br>

```
git branch -d 'name'
```

- ## Add modifications to a specific branch

<br>

```
git push –set-upstream origin 'name'
```

- ## Pull from specific branch

<br>

```
git pull origin 'branch name'
```

- ## Push to specific branch

<br>

```
git push origin 'branch name'
```

- ## Delete branch from local & remot

<br>

```
git push -d <remote_name> <branchname>
```

- ## Git config

<br>

- Set a Git username

```
git config --global user.name 'username'
```

- Set a Git username

```
git config --global user.email 'email'
```
