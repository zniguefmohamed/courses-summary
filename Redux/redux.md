# Redux

## state: initialData

## action : <br>

    .we use it to modify the state ``` don't modify the state directly ```
    . it is a JavaScript object
    . describe the reason for change in state
    . has property payload? 'optional'
    . we can use a function the create an action " Action Creator "

## producer : <br>

    . the brain of redux
    . here we will put the logic of the app
    . always have to use destructuring to change the state in a reducer

> ### producer is a function that recieves state & action as params and return a new state

<br>

```yaml
function reducer(state, action) {
// ...
}
```

<br>

<div align="center">
  <img src="./images/producer.png" width="450" >
</div>

<br>
<br>

## Store: <br>

    . it's what binds all the ingredients together
    . once the store created you can read the state using the function " store.getState(); "
    . to change the state we use " dispatch " ==> " store.setState(); " but we use dispatch not setState

<br>

> ### createStore function accepte two params the reducer & initialState

<br>

<div align="center">
  <img src="./images/redux_store.png" width="450">
</div>
