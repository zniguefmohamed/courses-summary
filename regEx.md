# Regular Expression Summary

```
pattern ==> / /

i ==> case insensitive (Zniguef = zniguef)

g ==> global

M ==> Multilines
```
<br>

## Ranges

<br>

```
let tld = "Com Net Org Code Io";

let tldRe = /(org|info|io)/i;

console.log(tld.match(tldRe)); //
```
## Letters Capital ans Small 

<br>

> [a-zA-Z]

<br>

### Numbers

<br>

> [0-9] 0 to 0

<br>

> [^0-9] Any Character Not 0 to 9

<br>

```
let nums = "12345678910";

let numsRe = /[0-9]/g;

console.log(nums.match(numsRe))
```

## Character Classes

<br>

```
( . )  : matche any character, exept new line or other line terminators

( \w ) : matche  word characters [a-z, A-Z, 0-9, and underscore]

( \W ) : matches  Non words characters

( \d ) : matches  digits from 0 to 9

( \D ) : matches  non digits characters

( \s ) : matches  whitespace character

( \S ) : matches non whitespace character

( \b ) : matche at the **beginning or end of the word

( \B ) : matche NOT at the beginnin or end of the word

```

## Test Method

the test method return **true or false**

### pattern.test(input)

<br>

## Quantifiers

<br>

```
( n+ )  => One Or More

( n* )  => Zero Or More

( n? )  => Zero Or One*



( n{x} )    => Number of

( n{x,y} )  => Range

( n{x,} )   => At last X



( $ )  => End with something

( ^ )  => Start with something

( ?= )  => Followed by something

( ?! )  => Not Followed by something


```
