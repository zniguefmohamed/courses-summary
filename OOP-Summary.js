// Object Literal
const book = {
title: "Book one",
author: "zniguef",
year: "2019",
getSummary: function() {
   return `${this.title} was written by ${this.author} in ${this.year}`;
}
}

// Constructor
function Book(title, author, year) {
    this.title = title;
    this.authoe = author;
    this.year = year;

    this.getSummary = function() {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
}

// Instatiate an object
const book1 = new Book ('book one',
"zniguef", 2019);

const book2 = new Book ("book two", "Mohamed", 2015);

console.log(book2.getSummary());

// Classes
class Book {
    constructor(title, author, year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    getSummary() {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    }
}

// Instantiate an object
const book_3 = new Book("book three", "me", "2011");
console.log(book_3);

// SubClasses

// Magazine subClass
class Magazine extends Book {
    constructor(title, author, year, month) {
        // super(): use it to call parent constructor and pass to it the original Book params
        super(title, author, year);
        this.month = month;
    }
}

// Instantiate magazine
const mag1 = new Magazine("mag one", "Zniguef Mohamed", "2015", "July");

console.log(mag1);
    