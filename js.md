# JavaScript Summary

### Arrow function
```
const sum = (a, b) => a + b
console.log(sum(2, 6))
// print 8
```

### Let Scope
```
let a = 3
if (true) {
let a = 5 \
console.log(a) // prints 5
}
console.log(a) // prints 3
```

### Template strings || (Template literal)
```
const name = 'World'
const message = `Hello ${name}`
console.log(message)
// prints "Hello World"
```

### Spread operator
```
const a = [ 1, 2 ]
const b = [ 3, 4 ]
const c = [ ...a, ...b ]
console.log(c) // [1, 2, 3, 4]
```

### Destructuring array
```
let [a, b] = [3, 7];
console.log(a); // 3
console.log(b); // 7
```

### Destructuring object
```
let obj = { a: 55, b: 44 };
let { a, b } = obj;
console.log(a); // 55
console.log(b); // 44
```

### Object property assignment
```
const a = 2
const b = 5
const obj = { a, b }
console.log(obj)
// prints { a: 2, b: 5 }
```

### Object.assign()
```
const obj1 = { a: 1 }
const obj2 = { b: 2 }
const obj3 = Object.assign({},
obj1, obj2)
console.log(obj3)
// { a: 1, b: 2 }
```

### Promises
```
promise
.then((result) => { ··· })
.catch((error) => { ··· })
```

### Destructuring Nested Objects
```
const Person = {
name: "Harry",
age: 25,
sex: "male",
materialStatus: "single",
address: {
country: "USA",
state: "Nevada",
city: "Carson City",
pinCode: "500014",
},
};
const { address : { state, pinCode }, name } = Person;
console.log(name, state, pinCode)  // Harry Potter Nevada 500014
console.log(city) // ReferenceError
```

## JavaScript loops

### for
```
for (let i = 0; i < 5; i++) {
 console.log(i);
}
//Prints 0 1 2 3 4
```

### do-while
```
let iterator = 0;
do {
 iterator++;
 console.log(iterator);
} while (iterator < 5);
//Prints 1 2 3 4 5
```

### while
```
let iterator = 0;
while (iterator < 5) {
 iterator++;
 console.log(iterator);
}
//Prints 1 2 3 4 5
```

### for...in (use it with object)
```
const arr = [3, 5, 7];
arr.foo = 'hello';
for (let i in arr) {
 console.log(i);
}
//Prints "0", "1", "2", "foo"
```

### for...of (use it with array)
```
const arr = [3, 5, 7];
for (let i of arr) {
 console.log(i);
}
//Prints 3, 5, 7
```

## JavaScript Arrays Destructuring

### Assigning array items to variables
```
const [a, b, c] = [123, 'second', true];
// a => 123
// b => 'second'
// c => true
```

### Assigning the first values, storing the rest together
```
const [a, b, ...rest] = [123, 'second', true, false, 42];
// a => 123
// b => 'second'
// rest => [true, false, 42]
```

### Swapping variables
```
let x = true;
let y = false;
[x, y] = [y, x];
// x => false
// y => true
```

## JavaScript Async/Await

### Async
When we append the keyword “async” to the function, this function \
returns the Promise by default on execution. Async keyword provides \
extra information to the user of the function:

> The function contains some Asynchronous Execution

> The returned value will be the Resolved Value for the Promise
```
async function f() {
 return 1;
}
f().then(alert); // 1
```

### Await
The keyword "await" makes JavaScript wait until that promise settles \
and returns its result

> The 'await' works only inside async functions

```
async function f() {
 let promise = new Promise((resolve, reject) => {
 setTimeout(() => resolve("done!"), 1000)
 });
 let result = await promise; // wait until the promise resolves
 alert(result); // "done!"
}
f();
```

## JavaScript API Calls

### Fetch
```
fetch('https://jsonplaceholder.typicode.com/todos'
).then(response => {
 return response.json();
}).then(data => {
 console.log(data);
})
```

### Axios
```
axios.get("https://jsonplaceholder.typicode.com/todos")
.then(response => {
 console.log(response.data)
})
```

