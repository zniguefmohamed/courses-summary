# Jav Summary

`Function in java`

```
ReturnType Name() {
    ...
}

function return anything

void Name() {
    ...
}
```

- Every java programme shoud have at laste one function `main()`
- access Modifiers `public - private - **Protected**`

<br>

`Class in Java`

```
public class Main {

    public void main() {

    }
}
```

<br>

## Types :

- `primitive types:` for storing simple values => `Numbers`, `Boolean `, `Characters`

- `reference types:` for storing complex objects => `Date`, `Email messages ...`

<br>
